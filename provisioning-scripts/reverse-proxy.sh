#!/bin/sh
####

echo ">>> Configuring reverse proxy"
a2enmod proxy proxy_http

cd /etc/apache2/sites-available/
cp 000-default.conf 000-default-BAK.conf

cat <<EOF >000-default.conf
<VirtualHost *:80>
	ProxyPreserveHost On
	ProxyRequests Off
	ProxyPass / http://localgov.lndo.site:8000/
	ProxyPassReverse / http://localgov.lndo.site:8000/
</VirtualHost>
EOF

systemctl restart apache2

echo ">>> "
echo ">>> To access the site in the browser, add the following entry to your hosts file:"
echo ">>> localgov.lndo.site		192.168.33.10"
echo ">>> "

