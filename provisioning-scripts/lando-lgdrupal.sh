#!/bin/sh

# Download and install lando
wget https://files.devwithlando.io/lando-stable.deb
sudo dpkg -i lando-stable.deb
rm lando-stable.deb

# Install local gov drupal using composer
composer create-project --stability dev localgovdrupal/localgov-project lgdrupal --remove-vcs 
