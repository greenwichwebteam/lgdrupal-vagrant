#!/bin/sh

# Update the base system
sudo apt-get update
sudo apt-get upgrade -y

# Install apache, php & required php modules
sudo apt-get install -y apache2 php php-xml php-mbstring php-gd php-curl

# Install unzip (used by composer)
sudo apt-get install -y unzip

# Download and install composer
EXPECTED_CHECKSUM="$(php -r 'copy("https://composer.github.io/installer.sig", "php://stdout");')"
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_CHECKSUM="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"
if [ "$EXPECTED_CHECKSUM" != "$ACTUAL_CHECKSUM" ]
then
	echo 'ERROR: Invalid installer checksum'
	rm composer-setup.php
	exit 1
fi
php composer-setup.php --quiet
rm composer-setup.php
sudo mv composer.phar /usr/local/bin/composer

# Download and run docker install script
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
rm get-docker.sh 

# Add vagrant user to docker group (and then reboot)
sudo usermod -aG docker $USER
